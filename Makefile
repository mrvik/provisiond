.PHONY: default all build go-install install uninstall clean

PREFIX:=usr/local
DESTDIR:=/
FLAGS:=-ldflags='-s -w' -trimpath -tags netgo
PKGNAME := provisiond

default: build

all: | clean build

build:
	go build -v ${FLAGS} -o ${PKGNAME}

go-install:
	go install -v ${FLAGS} .

install:
	install -Dm755 ${PKGNAME} ${DESTDIR}/${PREFIX}/bin/${PKGNAME}
	install -Dm644 services/provisiond.service ${DESTDIR}/${PREFIX}/lib/systemd/system/provisiond.service

uninstall:
	rm -f ${DESTDIR}/${PREFIX}/bin/${PKGNAME}
	rm -f ${DESTDIR}/${PREFIX}/lib/systemd/system/provisiond.service

clean:
	rm -f ${PKGNAME}
	go clean -v .

