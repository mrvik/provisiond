package server

import (
	"bytes"
	. "gitlab.com/mrvik/provisiond/types"
	"testing"
)

type TestingReq struct {
	sourceIP, path           string
	compareText, nilExpected bool
	expectedStatus           int
	expectedContentType      string
	expectedText             []byte
}

var (
	authorized   = []string{"127.0.0.1/8", "192.168.0.0/24", "192.168.1.100/32"}
	phonesConfig = map[string]*PhoneConfig{
		"default": {
			Server:              "10.0.1.1",
			AuthorizedAddresses: []string{"22.0.0.0/35"}, //Bad CIDR
		},
		"108F7A8B90AA": {
			Server:              "10.0.1.1",
			AuthorizedAddresses: authorized,
		},
		"76FFAC54D267": { //No AuthorizedAddresses section
			Server: "10.0.1.1",
			Config: "Just testing",
		},
	}
	reqs = []TestingReq{
		{
			sourceIP:            "192.168.0.2",
			path:                "/SEP76FFAC54D267.cnf.xml",
			nilExpected:         false,
			expectedStatus:      200,
			expectedText:        []byte(phonesConfig["76FFAC54D267"].Config),
			compareText:         true,
			expectedContentType: "application/xml",
		},
		{
			sourceIP:            "192.168.1.1",
			path:                "/SEP108F7A8B90AA.cnf.xml",
			nilExpected:         false,
			expectedStatus:      403,
			expectedContentType: "text/plain",
		},
		{
			sourceIP:    "127.0.1.1",
			path:        "/SEP108F7A8B90AA.cnf.xm", //Removed 'l' on purpose
			nilExpected: true,
		},
		{
			sourceIP:    "127.0.0.1",
			path:        "/SEP108F7A8B90A.cnf.xml", //Bad MAC
			nilExpected: true,
		},
	}
)

func TestGetValForMac(t *testing.T) {
	reqs := map[string]*PhoneConfig{"9586749598": phonesConfig["default"], "108F7A8B90AA": phonesConfig["108F7A8B90AA"]}
	for mac, expected := range reqs {
		rsp, err := getValForMac(mac, phonesConfig)
		if rsp != expected {
			t.Errorf("Got %v but %v expected\n", rsp, expected)
		} else {
			t.Logf("OK: Got %p for %s\n", rsp, mac)
		}
		if err != nil {
			t.Errorf("getValForMac returned an error for %s: %s\n", mac, err)
		}
	}
}

func TestIsIPAllowed(t *testing.T) {
	ips := map[string]bool{
		"127.0.0.1":       true,
		"192.168.0.1":     true,
		"10.0.0.1":        false,
		"192.168.1.100":   true,
		"192.168.255.2":   false,
		"256.256.900.100": false, //Invalid IP
	}
	t.Logf("Using %v\n", phonesConfig["108F7A8B90AA"].AuthorizedAddresses)
	for ip, expected := range ips {
		rt := isIPAllowed(ip, phonesConfig["108F7A8B90AA"])
		if rt != expected {
			t.Errorf("Got %t but %t expected for %s\n", rt, expected, ip)
		} else {
			t.Logf("OK: Got %t for %s\n", rt, ip)
		}
	}
	t.Logf("Using %v\n", phonesConfig["default"].AuthorizedAddresses)
	for ip := range ips {
		rt := isIPAllowed(ip, phonesConfig["default"]) //Has a bad AuthorizedAddresses field. Must return false
		if rt {
			t.Errorf("A bad config returned %t for %s\n", rt, ip)
		} else {
			t.Logf("OK: Got %t for %s\n", rt, ip)
		}
	}
	t.Logf("Using %v\n", phonesConfig["76FFAC54D267"].AuthorizedAddresses)
	for ip := range ips {
		rt := isIPAllowed(ip, phonesConfig["76FFAC54D267"])
		if !rt {
			t.Errorf("No AuthorizedAddresses limit but returned %t for %s\n", rt, ip)
		} else {
			t.Logf("OK: Got %t for %s\n", rt, ip)
		}
	}
}

func TestCanHandle(t *testing.T) {
	for _, v := range reqs {
		res := canHandle(v.path, v.sourceIP, phonesConfig)
		t.Logf("Testing \n\t%+v\nwith response\n\t%+v\n", v, res)
		if res == nil {
			if !v.nilExpected {
				t.Errorf("Got nil with %+v\n", v)
			}
		} else {
			if res.Status != v.expectedStatus {
				t.Errorf("Different status %d, expected %d for %+v\n", res.Status, v.expectedStatus, v)
			}
			if res.ContentType != v.expectedContentType {
				t.Errorf("Different content-type %s, expected %s for %+v\n", res.ContentType, v.expectedContentType, v)
			}
			if v.compareText && !bytes.Equal(v.expectedText, res.Content) {
				t.Errorf("Different bynary content for %+v\n", v)
			}
		}
	}
}

func BenchmarkCanHandle(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, v := range reqs {
			res := canHandle(v.path, v.sourceIP, phonesConfig)
			if res == nil && !v.nilExpected {
				b.Errorf("Nil not expected")
			}
		}
	}
}
