package server

import (
	"errors"
	. "gitlab.com/mrvik/provisiond/types"
	"log"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var macRegexp = regexp.MustCompile("[0-9A-F]{12}")

func Init(cnf *ConfigReceiver, stop chan bool) {
	log.Printf("Starting server on port %s", cnf.Addr)
	server := &http.Server{
		Addr: cnf.Addr,
	}
	defaultHandler := http.FileServer(http.Dir(cnf.StaticServe))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request for %q from %q\n", r.URL.Path, r.RemoteAddr)
		if strings.HasSuffix(r.URL.Path, "/") {
			w.WriteHeader(403)
			return
		}
		if response := canHandle(r.URL.Path, r.RemoteAddr, cnf.PhonesConfig); response != nil {
			w.Header().Add("Content-Type", response.ContentType)
			w.Header().Add("Content-Length", strconv.Itoa(len(response.Content)))
			w.WriteHeader(response.Status)
			w.Write(response.Content)
		} else {
			defaultHandler.ServeHTTP(w, r)
		}
	})
	wait := make(chan bool)
	go func() {
		server.ListenAndServe()
		wait <- true
	}()
	for {
		f := <-stop
		if f {
			log.Print("Closing server")
			server.Close()
			break
		}
	}
	<-wait
}

func canHandle(path, requestIP string, pc map[string]*PhoneConfig) *HandlerResponse {
	switch {
	case strings.HasPrefix(path, "/SEP") && strings.HasSuffix(path, ".cnf.xml"):
		{
			rp := HandlerResponse{ContentType: "application/xml", Status: 200}
			match := macRegexp.FindStringSubmatch(strings.Split(path, "/SEP")[1])
			if len(match) != 1 {
				return nil
			}
			mac := match[0]
			val, err := getValForMac(mac, pc)
			if err != nil {
				rp.ContentType = "text/plain"
				rp.Status = 404
				log.Printf("WARNING: There's no config for %s\n\t%s\n", mac, err)
				return &rp
			}
			if !isIPAllowed(requestIP, val) {
				rp.ContentType = "text/plain"
				rp.Status = 403
				log.Printf("Access denied to %q for %q\n", requestIP, path)
				return &rp
			}
			rp.Content = []byte(val.Config)
			return &rp
		}
	}
	return nil
}

func getValForMac(mac string, pc map[string]*PhoneConfig) (*PhoneConfig, error) {
	var rt *PhoneConfig
	var err error
	if val, ok := pc[mac]; ok {
		rt = val
	} else if val, ok = pc["default"]; ok {
		rt = val
	} else {
		err = errors.New("No config for " + mac)
	}
	return rt, err
}

func isIPAllowed(sourceIP string, val *PhoneConfig) bool {
	if len(val.AuthorizedAddresses) < 1 {
		return true //True if there's no filter
	}
	filteredIP := strings.Split(sourceIP, ":")
	if len(filteredIP) < 1 {
		return false //Invalid IP address
	}
	actualIP := net.ParseIP(filteredIP[0])
	for _, v := range val.AuthorizedAddresses {
		_, subnet, err := net.ParseCIDR(v)
		if err != nil {
			log.Printf("Error parsing AuthorizedAddresses: %s\n", err)
			return false //A bad config cannot authorize a connection
		}
		if subnet.Contains(actualIP) {
			return true
		}
	}
	return false
}
