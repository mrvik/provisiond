package main

import (
	"log"
	"os"

	"gitlab.com/mrvik/provisiond/server"
	"gitlab.com/mrvik/provisiond/signals"
	. "gitlab.com/mrvik/provisiond/types"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			log.Fatal("Stopping due to error")
		}
	}()
	config := generateConfig()
	stop := make(chan bool, 1)
	go signals.Watch(config, stop)
	server.Init(&config.Config, stop)
}

func generateConfig() *Configuration {
	if ln := len(os.Args); ln <= 1 {
		log.Fatalf("1 Argument is needed, %d provided\n", ln-1)
		return nil
	}
	path := os.Args[1]
	cnf := Configuration{ConfigPath: path}
	res := cnf.ReadConfig()
	if !res {
		log.Fatal("Cannot read config file")
		return nil
	}
	cnf.Config.ReadTemplates()
	return &cnf
}
