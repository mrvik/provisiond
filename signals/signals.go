package signals

import (
	. "gitlab.com/mrvik/provisiond/types"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func Watch(config *Configuration, stop chan bool) {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT, syscall.SIGUSR1)
	for {
		select {
		case value, ok := <-stop:
			{
				if ok && value {
					stop <- value //Resend signal
					break
				}
			}
		case value, ok := <-sigs:
			{
				if !ok {
					log.Fatalf("Signal handler not working properly")
				}
				switch value {
				case syscall.SIGUSR1:
					{
						config.ReadConfig()
						config.Config.ReadTemplates()
					}
				case syscall.SIGTERM, syscall.SIGINT:
					{
						stop <- true
					}
				}
			}
		}
	}
}
