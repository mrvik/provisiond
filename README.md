# Provisiond
A simple micro-service to provision your Cisco phones using a single template and a config file with the needed information.
This service is based on a single unified config file. This file shouldn't be world-readable as it has the usernames and passwords for all of the extensions configured.

## Config File
The supported formats are JSON and YAML. Detection has some caveats:
- If the file path has a ".json" termination, file will be interpreted as JSON.
- If the file path has a ".yml" termination, config will be interpreted as YAML.
- If none of these, provisiond will throw a warning and parse file as JSON.
Also, on YAML the properties are the same, but keys should be lowercase, otherwise they won't be used.

This file has some properties affecting the service (provisiond) and under PhonesConfig there are options for each phone.
- **Addr** string: Address to listen in. E.g 127.0.0.1:6970 to listen on loopback only or :6970 or 0.0.0.0:6970 to listen on all interfaces
- **StaticServe** string: Path where we should locate static files requested from the phones like firmwares or softkeys config
- **PhonesConfig**
    - **MAC** object: First level contains overall phone configurations. Replace MAC with the mac address of the phones or `default` as a fallback value
        - **AuthorizedAddresses** array of strings: An array of strings with source IP addresses allowed to get the config. Use the CIDR notation (E.g: 10.0.0.0/24).
        - **TemplatePath** string: Path to the template. Absolute or relative to the CWD of provisiond. See [Go docs for templates](https://golang.org/pkg/text/template/#hdr-Actions) to see how to write templates. Also you can take a look at the examples on the examples dir from repository.
        - **Server** string: Server address. Port included or not depends on the template you are writting.
        - **ProvisionID** string: Some sort of ID or address where some additional files like a centralized address book.
        - **Port** string: Server port to connect to.
        - **SecurePort** string: SIP secure port number.
        - **LoadInformation** string: Filename or path to .load file (without the .load extension).
        - **Config** none: Autopopulated when reading template. Any value will be _overwritten_.
        - **Label** string: Phone label
        - **Custom** map[string]string: Custom key-value to use on the config
        - **Lines** object array: Contains information for each line on the phone. May have multiple ones or just one
            - **Extension** string: SIP Extension to use.
            - **Name** string: Friendly name for extension.
            - **Username** string: User for authentication.
            - **Password** string: Password for authentication.
            - **Port** string: Port to connect. Not required unless spetial configuration requires it
            - **Proxy** string: SIP Proxy. Leave in blank if no proxy should be used.
            - **Custom** map[string]string: Custom key-value to use on the config

## The HTTP server
Cisco java-based phones try to connect to a HTTP service on 6970/tcp before trying tftp.
Provisiond reads config and compiles templates before starting the HTTP server.
Currently there are 2 handlers:
- If the name starts with SEP and ends on .cnf.xml. The request is processed like a provision request. This request is inmediately responded with the compiled configuration file (in memory). If there's no entry for the file, a 404 is returned.
- Else, the request is interpreted as a static file search in the StaticServe dir. Important or confidential files should be kept off this directory to prevent direct HTTP access.

## Configuration
Config is red on start, but can be modified and reloaded sending a SIGUSR1 to the process. This will force to read config file again and templates and recompile them, so the output can be changed without stopping and starting again the service (a phone may be updating firmware and download could be stopped on restart. Hot reload via SIGUSR1 fixes the problem).

## TFTP server
TFTP server has been forgotten because of this protocol is currently unneeded and is much slower than the HTTP server and the HTTP server is the first option for those phones.
In case your phone model needs it, you could use a tftp server like tftpd(from iputils)

## Runing Provisiond
If binding a non-privileged port (like the one that the java-based phones are expecting), any user can run it.
```
~> provisiond [configFile]
```
The configFile should be readable only by the user running the process as it contains very sensitive information like users and passwords.
A systemd unit is available under services/provisiond.service if you want to run it as a service on system start. On OpenRC and other init services you should know the stuff.

## Building from source
Dependencies:
- make
- go (makedepend)

