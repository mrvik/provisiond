package types

import (
	"os"
	"testing"
)

var config = Configuration{
	ConfigPath: "examples/test.json",
}

func TestReadConfig(t *testing.T) { //Function should be called first as os.Chdir is needed before any fs read is done
	os.Chdir("..") //By default go tests on the module's dir. The path is relative to the main package
	result := config.ReadConfig()
	if !result {
		t.Errorf("Cannot read config. Status: %t\n", result)
	} else {
		t.Logf("Config read OK. Status %t\n", result)
	}
}

func TestReadTemplates(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			switch val := r.(type) {
			case string, error:
				t.Error(val)
			default:
				t.Errorf("Unknown type for error %v (type %T)\n", r, r)
			}
		}
	}()
	result := config.Config.ReadTemplates()
	if !result {
		t.Errorf("Couldn't read templates. Status %t\n", result)
	} else {
		t.Logf("Templates read OK. Status %t\n", result)
	}
	for mac, phone := range config.Config.PhonesConfig {
		if phone == nil || phone.Config == "" {
			t.Errorf("Config on %q for %s not loaded\n", phone.TemplatePath, mac)
		}
	}
}
