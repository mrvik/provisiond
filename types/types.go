package types

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"strings"
	"text/template"

	"gopkg.in/yaml.v3"
)

type HandlerResponse struct {
	ContentType string
	Content     []byte
	Status      int
}

type Line struct {
	Number                                           int
	Extension, Name, Username, Password, Port, Proxy string
	Custom                                           map[string]string
}

type PhoneConfig struct {
	Label, Server, Port, SecurePort, ProvisionID, TemplatePath, LoadInformation string
	Custom                                                                      map[string]string
	Lines                                                                       []Line
	AuthorizedAddresses                                                         []string
	Config                                                                      string
}

type ConfigReceiver struct {
	PhonesConfig      map[string]*PhoneConfig
	Addr, StaticServe string
}

type Configuration struct {
	Config     ConfigReceiver
	ConfigPath string
}

func (c *Configuration) ReadConfig() bool {
	//This function should read file in ConfigPath(red from arguments when struct was created) and fill TemplatePath and Static Serve
	//Another function ReadTemplates should read TemplatePath file and populate or replace PhonesConfig array
	//Sighup should trigger this function and ReadTemplates again
	log.Print("Reading config")
	if c.ConfigPath == "" {
		log.Fatal("Config path cannot be empty")
		return false
	}
	cnfString, err := ioutil.ReadFile(c.ConfigPath)
	if err != nil {
		log.Fatal(err)
		return false
	}
	switch {
	case strings.HasSuffix(c.ConfigPath, ".json"):
		err = json.Unmarshal(cnfString, &c.Config)
	case strings.HasSuffix(c.ConfigPath, ".yml"):
		err = yaml.Unmarshal(cnfString, &c.Config)
	default:
		log.Println("Cannot find a known file type on file name. Defaulting to JSON")
		err = json.Unmarshal(cnfString, &c.Config)
	}
	if err != nil {
		log.Fatal(err)
		return false
	}
	log.Printf("Using config: %+v\n", *c)
	return true
}

func (c *ConfigReceiver) ReadTemplates() bool {
	var readyChannels []chan error
	for k, v := range c.PhonesConfig {
		if v.TemplatePath != "" {
			ready := make(chan error, 1)
			readyChannels = append(readyChannels, ready)
			go ReadTemplate(k, v, ready)
		}
	}
	for _, v := range readyChannels {
		rsp := <-v
		if rsp != nil {
			log.Panicf("Error while parsing templates:\n\t%s\n", rsp)
			return false
		}
	}
	return true
}

func ReadTemplate(mac string, pc *PhoneConfig, ready chan error) {
	content, err := ioutil.ReadFile(pc.TemplatePath)
	if err != nil {
		ready <- err
		return
	}
	//Read https://golang.org/pkg/text/template/ for template syntax
	var t *template.Template
	t, err = template.New(mac).Parse(string(content))
	if err != nil {
		ready <- err
		return
	}
	var tpl bytes.Buffer
	if err = t.Execute(&tpl, *pc); err != nil {
		ready <- err
		return
	}
	pc.Config = tpl.String()
	ready <- nil
}
